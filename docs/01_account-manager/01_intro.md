## Intro
API documentation for the account management app. The project goal is to bring user management for all apps to a centralized place.

The app should:
* Interact with the LDAP to check whether the user exists
* Create users on the LDAP
* CRUD users on the account_manager database
* Tell which client the user is coming from