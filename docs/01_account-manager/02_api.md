
```baseURL = http://sms.roamtech.com/accountmanager/public```

## Endpoints

#### Login (`POST /users/login`)
```javascript

// Request
{
	"username": "freesms",
	"password": "freesms",
	"clientId": 1,
}

// Response
200 OK
{
	"profile": {
		"email": "ken.murage+freesms@roamtech.com",
		"user": "freesms",
		"clientid": "19",
		"roles": "BULK"
	},
	"client": {
		"name": "smsleo",
		"api_key": "b665d4cfb214397050794ab3d69c6247",
		"salt": "666a391ed1e84b9b346b037ac271a6b969f9bb09",
		"call_back": "",
		"created": "2016-11-15 10:03:25"
	}
}

401 Authentication failed
{
	"code": 401,
	"message": "Invalid username or password"
}

```
#### Signup (`POST /users/new`)
```javascript
// Request
{
	"clientId": 1,
	"username": 'user',
	"password": 'password',
	"email": "email@address.com",
	"firstName": 'FirstName',
	"lastName": 'LastName',
	"phoneNumber": '0722123456'
	// * "profilePic": 'http://profile-pic.url',
	// * "roles": []
}

// Responses
201 Created 
{
	"user": [{
		"id":"7",
		"firstname":"FirstName",
		"lastname":"LastName",
		"email":"email@address.com",
		"phone_no":"0722123456",
		"created_by":"1",
		"created":null,
		"modified":"2016-11-28 14:43:23"
		"profile_pic":"http://profile-pic.url" // Automatically generates a gravatar from email if url isn't supplied
		}],
	"result":{
		"error":false,
		"code":201,
		"message":"user created successfully!"
		}
	}

400 Bad Request
{
	// Validation errors in an array
	"errors": [
		"first name required",
		"last name required",
		"password required",
		"email required",
		"user exists"
		],
	"message":"Check errors"
}

500 Server Error // Any error that happens on the DB is supplied through this code

{
	"error": true,
	"code": 500, // Specific error code might be here
	"message": {{ error message from db }}
}

```